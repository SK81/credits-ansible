# credits-ansible

## Install requirements
### Type the following apt-get command or apt command:
    $ sudo apt update
    $ sudo apt upgrade
    $ sudo apt install software-properties-common

### Add ppa:ansible/ansible to your system’s Software Source:
    $ sudo apt-add-repository ppa:ansible/ansible

### Update your repos:
    $ sudo apt update

### To install the latest version of ansible, enter:
    $ sudo apt install ansible
    
### Install python-netaddr module (for find private/external IP)
    $ sudo apt install python-netaddr
    
## Install playbooks from git    

    $ mkdir dev
    $ cd dev/
    $ git clone https://gitlab.com/SK81/credits-ansible.git

## Edit file production, write node name, signal server name and external IP address
    #node section
    [nodes] 
    "hostname"  ansible_host="external_IP" ansible_connection=ssh ansible_user={{vault_user}} ansible_ssh_pass={{vault_pass}}
    
    #signal server section
    [signals]
    "hostname"  ansible_host="external_IP" ansible_connection=ssh ansible_user={{vault_user}} ansible_ssh_pass={{vault_pass}}


## Set username and pass in security storage 

    ansible-vault edit group_vars/vault.yml

    vault_user: "ssh_username"
    vault_pass: "ssh_password"
    ansible_become_pass: "sudo_password"


## Setup blockchain

    $ ansible-playbook -i production  --ask-vault-pass -e @group_vars/vault.yml site.yml




